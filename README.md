# Set up a watchdog on network ping using Raspberry PI 3 with Raspbian Jessie  #
In case you need to reset your Raspberry PI when network connection is lost! For example 

* Version 0.1
* Written by Guido Giorgetti

## What is it for? ##
* It's for setting a watchdog to reset (reboot) your Raspberry (with Raspbian Jessie) when network connection is lost.

## Summary ##

A service (watchdog.service) handled by [systemd](https://wiki.debian.org/systemd) checks:
 
* IF a ping (to the LAN router or any IP host you want) succeeds 
* AND the Raspberry has been up for more than 120 secs
* THEN it restarts the watchdog timer and nothing else happens 
* ELSE the watchdog timer sends a reset to the Raspberry
* REPEAT after 10 second

![alt tag](https://bytebucket.org/Guido28/network-ping-watchdog-for-raspberry/raw/b5616ad06851a83fc8518d49db83696f30aaa388/images/flowchart.JPG)

## Steps to set it up in a Raspberry PI 3 with Debian Jessie ##

1) Install the kernel watchdog module: **sudo modprobe bcm2835_wdt** 

2) Install the watchdog program: **sudo apt-get install watchdog chkconfig**

3)  Copy the script_files to an appropriate directory. I used /home/webide/repositories/prova/folderprova/  

   * uptime.sh  # a simple bash script called by watchdog.service (see repair-binary just above) 
   * uptime.py  # a python script which returns -1 if you need to reboot (ping is KO AND uptime > 120 secs); 0 (don't reboot) otherwise

4) Modify /etc/watchdog.config cloning the config_files/watchdog.config
      These are the relevant lines:

```
#!
      ### 15 seconds is the maximum "watchdog-timeout" value for Raspberry 
      watchdog-timeout = 14
      ### Call the watchdog (pat the dog) at least every 10 seconds (10 secs < 14 secs ok?) 
      interval = 10
      ### log the watchdog every 180 seconds 
      logtick = 180
      ### the IP address to ping. In my case, it is my LAN router address, which is enough to be sure that the network is working! 
      ping			= 192.168.1.1
      ### The bash script to call if ping is KO!
      ### It returns 0 (OK=don't reboot) if uptime < 180 seconds; otherwise it returns -1 (KO=reboot!)
      ### This check is mandatory because when Raspberry boots, networking takes some time (20-60 seconds) to start working! Don't forget to change the directory address where you placed the script files (see step 3)
      repair-binary		= /home/webide/repositories/prova/folderprova/uptime.sh
      ### the repair binary must return in less than 5 seconds, otherwise Raspberry reboots all the same!
      repair-timeout		= 5
```
 
5) Start the systemd watchdog.service: **sudo systemctl start watchdog**

6) Check if watchdog started: **systemctl status watchdog**
   
   If everything's OK you should see:
      
      ...
      watchdog.service - watchdog daemon
      Loaded: loaded (/lib/systemd/system/watchdog.service; enabled)
      Active: active (running) since Sun 2017-01-08 19:17:09 CET; 1h 50min ago
      ...   
7) Try to **unconnect the Ethernet plug**, to simulate a network failure 

8) (Optional) If you need to modify watchdog.config do like this:

  * **sudo systemctl stop watchdog**    stop the watchdog service

  * **sudo nano watchdog.config**       edit watchdog.config

  * **sudo systemctl start watchdog** (re)start the watchdog service

  * **systemctl status watchdog** (verify if the service started correctly (see 6.)