"""
    argument: error number 
    
    return  0 if uptime is less than uptime_limit
    return -1 otherwise

"""

import os
import sys
import time

uptime_limit = 180 # seconds

def main():
    
   
    if len(sys.argv) > 1:
        err_num = sys.argv[1]
        log_write( "err_num="+err_num) 
        print("err_num="+err_num)
      
    uptime_file = open("/proc/uptime","r")
    uptime = uptime_file.read().split()[1]
    print ("uptime="+uptime)
    log_write("uptime="+uptime) 
        
    uptime_file.close()

    if float(uptime) < uptime_limit or uptime_limit == 0:
        log_write("return 0") 
        sys.exit(0)  # 0 means OK    
    else:
        log_write( "return -1") 
        sys.exit(-1)  # -1 means reboot    
     

def log_write(str):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    now = time.strftime("%Y-%m-%d %H:%M:%S")
    log_file = open(dir_path+"/log","a+")
    log_file.write(now + " " + str+"\n") 
    log_file.close()   
    return


if __name__ == "__main__":
    main()
    
